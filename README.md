# Amazon Kinesis Data Streams Connector
Amazon Kinesis Data Streams is a managed service that scales elastically for real-time processing of streaming big data.

Documentation: https://docs.aws.amazon.com/kinesis/

Specification: https://raw.githubusercontent.com/APIs-guru/openapi-directory/main/APIs/amazonaws.com/kinesis/2013-12-02/openapi.yaml

## Prerequisites

+ Your kinesis server, matching the pattern https://kinesis.{region}.{domain}. Example: https://kinesis.us-east-1.amazonaws.com
+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
**All end points are passing.**
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

